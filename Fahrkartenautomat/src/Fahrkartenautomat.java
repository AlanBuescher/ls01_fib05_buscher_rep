﻿import java.util.Scanner;

class Fahrkartenautomat {

	public static void main(String[] args) {

		Scanner tastatur = new Scanner(System.in);

		double gesamtPreis = fahrkartenbestellungErfassen();
		double rückGeld = fahrkartenBezahlen(gesamtPreis);
		fahrkartenAusgeben();
		rueckgeldAusgeben(rückGeld);
	}

	public static double fahrkartenbestellungErfassen() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Wie viele Fahrkarten werden gekauft?");
		int anzahlTickets = tastatur.nextInt();

		System.out.println("Wie viel kostet eine Fahrkarte?");
		double einzelPreis = tastatur.nextDouble();
		double gesamtPreis = anzahlTickets * einzelPreis;
		return gesamtPreis;
	}

	public static double fahrkartenBezahlen(double gesamtPreis) {
		// Geldeinwurf
		// ---------
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0;
		while (eingezahlterGesamtbetrag < gesamtPreis) {
			double zuZahlenderBetrag = gesamtPreis - eingezahlterGesamtbetrag;
			zuZahlenderBetrag = Math.round(100D * zuZahlenderBetrag) / 100D;
			System.out.printf("Noch zu zahlen: %.2f EURO", zuZahlenderBetrag);
			System.out.print(" Eingabe (mind. 5 Cent, höchstens 100 Euro): ");
			double eingeworfeneMünze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMünze;
		}
		return eingezahlterGesamtbetrag - gesamtPreis;
	}

	public static void fahrkartenAusgeben() {
		// Fahrscheinausgabe
		// -----------------
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n");
		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wünschen Ihnen eine gute Fahrt.\n");
	}

	public static void rueckgeldAusgeben(double rückGeld) {
		// Rückgeldberechnung und -Ausgabe
		// -------------------------------
		rückGeld += 0.0001;
		if (rückGeld > 0.00) {
			System.out.printf("Der Rückgeld Betrag ist in Höhe von %.2f EURO", rückGeld);
			System.out.println(" wird in folgenden Münzen ausgezahlt:");
			while (rückGeld >= 100.0) {
				System.out.println("100 EURO");
				rückGeld -= 100.0;
			}
			while (rückGeld >= 50.0) {
				System.out.println("50 EURO");
				rückGeld -= 50.0;
			}
			while (rückGeld >= 20.0) {// 20 EURO-Schein
				System.out.println("20 EURO");
				rückGeld -= 20.0;
			}
			while (rückGeld >= 10.0) // 10 EURO-Schein
			{
				System.out.println("10 EURO");
				rückGeld -= 10.00;
			}
			while (rückGeld >= 5.0) // 2 EURO-Schein
			{
				System.out.println("5 EURO");
				rückGeld -= 5.00;
			}
			while (rückGeld >= 2.0) // 2 EURO-Münzen
			{
				System.out.println("2 EURO");
				rückGeld -= 2.00;
			}
			while (rückGeld >= 1.0) // 1 EURO-Münzen
			{
				System.out.println("1 EURO");
				rückGeld -= 1.00;
			}
			while (rückGeld >= 0.50) // 50 CENT-Münzen
			{
				System.out.println("50 CENT");
				rückGeld -= 0.50;
			}
			while (rückGeld >= 0.20) // 20 CENT-Münzen
			{
				System.out.println("20 CENT");
				rückGeld -= 0.20;
			}
			while (rückGeld >= 0.10) // 10 CENT-Münzen
			{
				System.out.println("10 CENT");
				rückGeld -= 0.10;
			}
			while (rückGeld >= 0.05)// 5 CENT-Münzen
			{
				System.out.println("5 CENT");
				rückGeld -= 0.05;
			}
			while (rückGeld >= 0.02) {// 2 CENT-Münzen
				System.out.println("2 CENT");
				rückGeld -= 0.02;
			}
			while (rückGeld >= 0.01) {// 1 CENT-Münzen
				System.out.println("1 CENT");
				rückGeld -= 0.01;
			}
			System.exit(5);
		}
	}
}
