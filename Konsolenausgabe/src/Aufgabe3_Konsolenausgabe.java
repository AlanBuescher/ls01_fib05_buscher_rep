
public class Aufgabe3_Konsolenausgabe {

	public static void main(String[] args) {
		String grenze = "-----------------------\n";
		String uebersch = "Fahrenheit |  Celsius \n";
		String f1 = "-20        ";
		String f2 = "-10        ";
		String f3 = "0          ";
		String f4 = "20         ";
		String f5 = "30         ";
		String c1 = "|   -28.88 \n";
		String c2 = "|   -23.33 \n";
		String c3 = "|   -17.77 \n";
		String c4 = "|   -6.66 \n";
		String c5 = "|   -1.11 \n";
		
		
		System.out.printf("%s", uebersch);
		System.out.printf("%s", grenze);
		System.out.printf("%-5d", f1);// Fahrenheit
		System.out.printf("%10s", c1);// Celsius
		System.out.printf("%-5d", f2);// Fahrenheit
		System.out.printf("%10s", c2);// Celsius
		System.out.printf("%-5d", f3);// Fahrenheit
		System.out.printf("%10s", c3);// Celsius
		System.out.printf("%-5d", f4);// Fahrenheit
		System.out.printf("%10s", c4);// Celsius
		System.out.printf("%-5d", f5);// Fahrenheit
		System.out.printf("%10s", c5);// Celsius
	 }
	
}
